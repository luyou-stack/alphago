import random
from copy import deepcopy

class Go:
    def __init__(self):
        file_string = ''
        with open('input.txt', 'r') as file:
            file_string = file.read().split()
        self.piece_type = int(file_string[0])
        previous_board_string = file_string[1:6]
        board_string = file_string[6:]
        self.previous_board = [[0]*5 for i in range(5)]
        self.board = [[0]*5 for i in range(5)]
        for y in range(5):
            for x in range(5):
                self.previous_board[y][x] = int(previous_board_string[y][x])
                self.board[y][x] = int(board_string[y][x])

class MyPlayer:

    def __init__(self):
        self.type = 'Genius'
        self.max_depth = 3

    def liberties(self, board, y, x):
        ans = 0
        if y > 0 and board[y - 1][x] == 0: ans += 1
        if y < 4 and board[y + 1][x] == 0: ans += 1
        if x > 0 and board[y][x - 1] == 0: ans += 1
        if x < 4 and board[y][x + 1] == 0: ans += 1
        return ans

    def group_liberties(self, board, y, x, piece_type):
        ans = 0
        board[y][x] = 3
        if y > 0:
            if board[y - 1][x] == 0:
                ans += 1
            elif board[y - 1][x] == piece_type:
                ans += self.group_liberties(board, y - 1, x, piece_type)
        if y < 4:
            if board[y + 1][x] == 0:
                ans += 1
            elif board[y + 1][x] == piece_type:
                ans += self.group_liberties(board, y + 1, x, piece_type)
        if x > 0:
            if board[y][x - 1] == 0:
                ans += 1
            elif board[y][x - 1] == piece_type:
                ans += self.group_liberties(board, y, x - 1, piece_type)
        if x < 4:
            if board[y][x + 1] == 0:
                ans += 1
            elif board[y][x + 1] == piece_type:
                ans += self.group_liberties(board, y, x + 1, piece_type)
        return ans

    def legal(self, board, y, x, piece_type):
        if board[y][x] != 0: return False
        another_piece_type = 1 if piece_type == 2 else 2
        if self.group_liberties(deepcopy(board), y, x, piece_type):
            return True
        else:
            if y > 0 and board[y - 1][x] == another_piece_type and self.group_liberties(deepcopy(board), y - 1, x,
                                                                                        another_piece_type) == 1: return True
            if y < 4 and board[y + 1][x] == another_piece_type and self.group_liberties(deepcopy(board), y + 1, x,
                                                                                        another_piece_type) == 1: return True
            if x > 0 and board[y][x - 1] == another_piece_type and self.group_liberties(deepcopy(board), y, x - 1,
                                                                                        another_piece_type) == 1: return True
            if x < 4 and board[y][x + 1] == another_piece_type and self.group_liberties(deepcopy(board), y, x + 1,
                                                                                        another_piece_type) == 1: return True
        return False

    def update_board(self, board, y, x, piece_type):
        if x == -1: return board  # -1 means pass
        copied_board = deepcopy(board)
        copied_board[y][x] = piece_type
        ans = deepcopy(copied_board)
        another_piece_type = 1 if piece_type == 2 else 2
        for y in range(5):
            for x in range(5):
                if copied_board[y][x] == another_piece_type and self.group_liberties(deepcopy(copied_board), y, x, another_piece_type) == 0:
                    ans[y][x] = 0
        return ans

    def alpha_beta_pruning(self):
        pass

    def score(self, board, piece_type):
        total_liberties = self.total_liberties(board, piece_type)
        count = self.count(board, piece_type)
        return total_liberties[0] - total_liberties[1] + count[0] - count[1]

    def max_total_score(self, previous_board, board, piece_type, alpha, beta, depth):
        if depth == self.max_depth:
            return self.score(board, piece_type), -1, -1
        V, Y, X = -1000, -1, -1
        point_list = sum([[(y, x) for x in range(5)] for y in range(5)], [])
        random.shuffle(point_list)
        for y, x in point_list:
            if previous_board[y][x] == piece_type: continue
            if self.legal(board, y, x, piece_type):
                updated_board = self.update_board(board, y, x, piece_type)
                v = self.min_total_score(board, updated_board, piece_type, alpha, beta, depth+1)[0]
                if v > V: V, Y, X = v, y, x
                if v >=beta: return v, y, x
                if v > alpha: alpha = v
        return V, Y, X


    def min_total_score(self, previous_board, board, piece_type, alpha, beta, depth):
        if depth == self.max_depth:
            return self.score(board, piece_type), -1, -1
        another_piece_type = 1 if piece_type == 2 else 2
        V, Y, X = 1000, -1, -1
        point_list = sum([[(y, x) for x in range(5)] for y in range(5)], [])
        random.shuffle(point_list)
        for y, x in point_list:
            if previous_board[y][x] == another_piece_type: continue
            if self.legal(board, y, x, another_piece_type):
                updated_board = self.update_board(board, y, x, another_piece_type)
                v = self.max_total_score(board, updated_board, piece_type, alpha, beta, depth+1)[0]
                if v < V: V, Y, X = v, y, x
                if v <= alpha: return V, Y, X
                if v < beta: beta = v
        return V, Y, X


    def total_liberties(self, board, piece_type):
        ans0 = 0
        ans1 = 0
        another_piece_type = 1 if piece_type == 2 else 2
        for y in range(5):
            for x in range(5):
                if board[y][x] == piece_type:
                    ans0 += self.liberties(board, y, x)
                elif board[y][x] == another_piece_type:
                    ans1 += self.liberties(board, y, x)
        return ans0, ans1

    def count(self, board, piece_type):
        ans0 = 0
        ans1 = 0
        another_piece_type = 1 if piece_type == 2 else 2
        for y in range(5):
            for x in range(5):
                if board[y][x] == piece_type:
                    ans0 += 1
                elif board[y][x] == another_piece_type:
                    ans1 +=1
        return ans0, ans1

    def get_input(self, go, piece_type):
        board = go.board
        previous_board = go.previous_board
        V, Y, X = self.max_total_score(previous_board, board, piece_type, -1000, 1000, 0)
        if X == -1:
            return 'PASS'
        else:
            return Y, X

if __name__ == '__main__':
    go = Go()
    agent = MyPlayer()
    decision = agent.get_input(go, go.piece_type)
    with open('output.txt', 'w') as file:
        if decision == 'PASS':
            file.write('PASS')
        else:
            file.write(str(decision[0]))
            file.write(',')
            file.write(str(decision[1]))
