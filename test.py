import time


from host import GO
from random_player import RandomPlayer
from my_player3 import MyPlayer


N = 5

print("Alexa_lu uses MCTS")

n = 100
w0 = 0
w1 = 0
w2 = 0
start = time.time()
print("Alexa_lu vs RRANDOM PLAYER 200 rounds\n")
for i in range(n):
    go = GO(N)
    player1 = MyPlayer()
    player2 = RandomPlayer()
    result = go.play(player1, player2, False)
    if result == 1: w1 += 1
    print(result)
print('Alexa_lu的先手胜率：')
print(w1/n)
print()
for i in range(n):
    go = GO(N)
    player1 = RandomPlayer()
    player2 = MyPlayer()
    result = go.play(player1, player2, False)
    if result == 2: w2 += 1
    print(result)
print('Alexa_lu的后手胜率：')
print(w2/n)
print()
print('Alexa_lu的总体胜率：')
print((w1+w2)/n/2)

finish = time.time()
print()
print('time used')
print(finish-start)
print()
