Content:
This is  an AlphaGo agent with more than a 80% win rate to play a Go against players of increasing difficulty by using a hybrid method
involving minimax search with alpha-beta pruning.

Why use this agent?
Because this agent has a winning rate of 80% when competing with other agents, and the code is very readable.

How to use this agent?
My_player3.py and random_player are the agents.
Test.py is used for local testing.
Host.py is the judge of the game.
Write.py and read.py are used for reading and write file.
